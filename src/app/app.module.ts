import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
// fusion charts
import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as charts from 'fusioncharts/fusioncharts.charts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as PowerChart from 'fusioncharts/fusioncharts.powercharts';
FusionChartsModule.fcRoot(FusionCharts, charts, FusionTheme, PowerChart);
// components
import { HeaderComponent } from './components/header/header.component';
import { AppComponent } from './app.component';
import { GraphComponent } from './components/graph/graph.component';
import { TaskBlockComponent } from './components/task-block/task-block.component';
import { DialogNodeComponent } from './components/dialog-node/dialog-node.component';
import { FooterComponent } from './components/footer/footer.component';
import { DialogViewTaskComponent } from './components/task-block/dialog-view-task/dialog-view-task.component';
import { DialogViewResWorkComponent } from './components/dialog-view-res-work/dialog-view-res-work.component';




@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    HeaderComponent,
    TaskBlockComponent,
    DialogNodeComponent,
    FooterComponent,
    DialogViewTaskComponent,
    DialogViewResWorkComponent
  ],
  imports: [
    MatSlideToggleModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatButtonModule,
    FusionChartsModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
