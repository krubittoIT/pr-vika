export class DialogData {
    public idNode: number;
    public nameNode: string;
    public listNodes: ListNodeData[];
}

export class ResultAddConnector {
    public valueConnector: number;
    public Node: ListNodeData;
}

export class ListNodeData {
    public idNode: number;
    public nameNode: string;
}
