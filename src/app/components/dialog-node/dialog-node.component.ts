import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData, ResultAddConnector, ListNodeData } from './model';

@Component({
  selector: 'app-dialog-node',
  templateUrl: './dialog-node.component.html',
  styleUrls: ['./dialog-node.component.scss']
})
export class DialogNodeComponent implements OnInit {

  public NodeWithConnector: ListNodeData;
  public ValueWithConnector: number;

  constructor(
    public dialogRef: MatDialogRef<DialogNodeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
    // console.log('selected node into modal', this.data);
  }

  closeDialog(res: ResultAddConnector) {
    // console.log('close', res);
    this.dialogRef.close(res);
  }

  applyDialog() {
    const result: ResultAddConnector = {
      valueConnector: this.ValueWithConnector,
      Node: this.NodeWithConnector
    };
    // console.log('apply', result);
    this.closeDialog(result);
  }

}
