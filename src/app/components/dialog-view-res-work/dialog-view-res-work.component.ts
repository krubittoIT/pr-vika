import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ResultWork } from '../graph/model';

@Component({
  selector: 'app-dialog-view-res-work',
  templateUrl: './dialog-view-res-work.component.html',
  styleUrls: ['./dialog-view-res-work.component.scss']
})
export class DialogViewResWorkComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogViewResWorkComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ResultWork[]) { }

  ngOnInit() {
  }

}
