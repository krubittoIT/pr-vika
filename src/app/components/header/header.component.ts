import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public pathLogIcon = './assets/logo.png';
  public title = 'Алгоритм Беллмана-Форда нахождения кратчайшего пути от отдной вершины графа до всех остальных во взвешенном графе';
  public subTitle = 'Алгоритм Беллмана-Форда нахождения кратчайшего пути';
  public subSubTitle = 'Алгоритм Беллмана-Форда';
  public pathLogSamUnivIcon = './assets/logo-ru.png';
  public width = 0;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.width = window.innerWidth;
    this.eventService.resizeWidth
      .subscribe(
        next => {
          // console.log(next);
          this.width = next;
        }
      );
  }

}
