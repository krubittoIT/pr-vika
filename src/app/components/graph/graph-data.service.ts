import { Injectable } from '@angular/core';
import { Dataset, ConnectorsGraphData, EnumGraphArcState, ObjEventClicNode, ResultWork } from './model';
import { ListNodeData } from '../dialog-node/model';

const listNameGraph = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

@Injectable()
export class GraphDataService {
    public static listNameGraph = listNameGraph;

    constructor() { }

    public static getDataSource(mydataset: Dataset[], myconnectors: ConnectorsGraphData[]) {
        return {
            chart: {
                // theme
                theme: 'fusion',
                // border
                plotborderthickness: '4',
                showPlotBorder: '1',
                inheritPlotBorderColor: '1',
                // canvas
                yAxisMaxValue: '1000',
                yAxisMinValue: '0',
                xAxisMaxValue: '1000',
                xAxisMinValue: '0',
                // font
                baseFontSize: 20,
                baseFontColor: '#000000',
                // menu
                viewMode: '1',
                // arrow
                arrowatstart: '1',
                arrowatend: '1',
                // tool text
                connectorToolText: '$label',

            },
            dataset: mydataset,
            connectors: myconnectors
        };
    }

    public static getChartConfig() {
        return {
            width: '100%',
            height: '100%',
            type: 'dragnode',
            dataFormat: 'json'
        };
    }

    public static getMaxCountNode(): number {
        return this.listNameGraph.length;
    }

    public static addConnectorToNode(connectors: ConnectorsGraphData[], idFrom: number,
        idTo: number, valueCon: number, stateArc: EnumGraphArcState) {
        if (connectors.length === 0) {
            connectors.push({
                connector: [],
                stdthickness: 'con'
            });
        }
        if (connectors[0].connector.length > 0) {
            connectors = this.checkAndRemoveConnect(connectors, idFrom, idTo, stateArc);
        }
        switch (stateArc) {
            case EnumGraphArcState.Directed: {
                connectors[0].connector.push({
                    from: '' + idFrom,
                    to: '' + idTo,
                    color: '#1aaf5d',
                    strength: '2',
                    arrowatstart: '0',
                    arrowatend: '1',
                    alpha: '100',
                    label: '' + valueCon
                });
                break;
            }
            case EnumGraphArcState.NonDirected: {
                connectors[0].connector.push({
                    from: '' + idFrom,
                    to: '' + idTo,
                    color: '#1aaf5d',
                    strength: '2',
                    arrowatstart: '0',
                    arrowatend: '0',
                    alpha: '100',
                    label: '' + valueCon
                });
                connectors[0].connector.push({
                    from: '' + idTo,
                    to: '' + idFrom,
                    color: '#1aaf5d',
                    strength: '2',
                    arrowatstart: '0',
                    arrowatend: '0',
                    alpha: '100',
                    label: '' + valueCon
                });
                break;
            }
        }

    }

    public static doWork(dataset: Dataset[], connectors: ConnectorsGraphData[],
        idNodeStart: number, countNode: number, countArc: number): ResultWork[] {
        const listArc = connectors[0].connector;
        const d: ResultWork[] = [];
        let x;
        for (let i = 0; i < countNode; i++) {
            d.push({
                value: '10000000',
                nameNode: this.getNameNodeById(dataset, i + 1)
            });
            // d[i].value = 1000000;
        }
        d[idNodeStart].value = '0';
        // console.log('start', d);

        for (let i = 0; i < countNode - 1; i++) {
            // console.log(x);
            x = -1;
            for (let j = 0; j < countArc; j++) {
                if (+d[+listArc[j].from - 1].value + +listArc[j].label < +d[+listArc[j].to - 1].value) {
                    d[+listArc[j].to - 1].value = '' + (+d[+listArc[j].from - 1].value + +listArc[j].label);
                    x = +listArc[j].to;
                }
            }
        }
        if (x !== undefined) {
            if (x === -1) {
                alert('nope negative cycle');
            } else {
                alert('here negative cycle');
            }
        }
        // console.log('end', d);
        d.splice(idNodeStart, 1);
        d.forEach(element => {
            element.value = element.value === '10000000' ? 'нет пути' : element.value;
        });
        return d;
    }

    private static getNameNodeById(dataset: Dataset[], id: number): string {
        const resultSearch = dataset[0].data.find((x) => +x.id === id);
        return resultSearch.name;
    }

    private static checkAndRemoveConnect(
        connectors: ConnectorsGraphData[], idFrom: number, idTo: number, stateArc: EnumGraphArcState): ConnectorsGraphData[] {

        switch (stateArc) {
            case EnumGraphArcState.Directed: {
                const resSearch = connectors[0].connector.findIndex((x) => (+x.from === idFrom && +x.to === idTo) ||
                    (+x.from === idTo && +x.to === idFrom));
                if (resSearch !== -1) {
                    connectors[0].connector.splice(resSearch, 1);
                }
                break;
            }
            case EnumGraphArcState.NonDirected: {
                // console.log(connectors);
                const firstArcId = connectors[0].connector.findIndex((x) => +x.from === idFrom && +x.to === idTo);
                const secondArcId = connectors[0].connector.findIndex((x) => +x.from === idTo && +x.to === idFrom);
                if (firstArcId !== -1 && secondArcId !== -1) {
                    connectors[0].connector.splice(firstArcId, 1);
                    connectors[0].connector.splice(secondArcId, 1);
                }
                break;
            }
        }
        return connectors;
    }

    public static getListAllNodeForCreateArc(dataset: Dataset[], selectedNode: ObjEventClicNode): ListNodeData[] {
        const result: ListNodeData[] = [];
        dataset[0].data.forEach(element => {
            if (+element.id !== selectedNode.idNode) {
                result.push({
                    idNode: +element.id,
                    nameNode: element.name
                });
            }
        });
        return result;
    }

    public static changePosNode(dataset: Dataset[], newPosX: number, newPosY: number, idDragNode: number) {
        const searchNode = dataset[0].data.find((x) => +x.id === idDragNode);
        if (searchNode) {
            searchNode.x = newPosX;
            searchNode.y = newPosY;
        }
    }


    public static addNewNode(dataset: Dataset[]) {
        dataset[0].data.push(
            {
                id: '' + (dataset[0].data.length + 1),
                name: getNextNameNode(dataset[0].data.length),
                radius: '30',
                shape: 'CIRCLE',
                // color: '#ffffff',
                x: 62,
                y: 950,
            }
        );
    }

    public static addNewNodeOnPosition(dataset: Dataset[], x: number, y: number) {
        dataset[0].data.push(
            {
                id: '' + (dataset[0].data.length + 1),
                name: getNextNameNode(dataset[0].data.length),
                radius: '30',
                shape: 'CIRCLE',
                x,
                y,
            }
        );
    }

    public static removeAllNode(dataset: Dataset[], connectors: ConnectorsGraphData[]) {
        dataset[0].data = [];
        connectors[0].connector = [];
    }
}

function getRandomPositionNewNode(positionLastNOde: number): number {
    const change = Math.floor(Math.random() * Math.floor(1000));
    const randC = Math.floor(Math.random() * Math.floor(2));
    let newVal = 1;
    switch (randC) {
        case 0: {
            newVal = positionLastNOde + change;
            break;
        }
        case 1: {
            newVal = positionLastNOde - change;
            break;
        }
    }
    return change;
}

function getNextNameNode(count: number): string {
    return listNameGraph[count];
}
