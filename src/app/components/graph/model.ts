export class Dataset {
    public id: string;
    public seriesname: string;
    public data: Data[];
}

export class Data {
    public id: string;
    public name: string;
    public radius: string;
    public shape: string;
    public color?: string;
    public x?: number;
    public y?: number;
    public tooltext?: string;
}

export class ConnectorsGraphData {

    public stdthickness?: string;
    public connector: ConnectorGraph[];
}

export class ConnectorGraph {
    public from: string;
    public to: string;
    public color: string;
    public strength: string;
    public arrowatstart: string;
    public arrowatend: string;
    public alpha: string;
    public label: string;
}

export class FusionChartsEvent {
    public dataObj: {
        id: string,
        label: string,
        x: number
        y: number
    };
    public eventObj: any;
}

export class ObjEventClicNode {
    public idNode: number;
    public nameNode: string;
}

export class ResultWork {
    public value: string;
    public nameNode: string;
}

export class ObjEventClicOnChart {
    constructor(
        public x: number,
        public y: number) { }
}

export enum EnumGraphArcState {
    Directed = 0,
    NonDirected = 1
}



