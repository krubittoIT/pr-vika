import { Component, OnInit } from '@angular/core';
import { Dataset, ConnectorsGraphData, FusionChartsEvent, ObjEventClicNode, ObjEventClicOnChart, EnumGraphArcState, ResultWork } from './model';
import { GraphDataService } from './graph-data.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogNodeComponent } from '../dialog-node/dialog-node.component';
import { DialogData, ResultAddConnector } from '../dialog-node/model';
import { DialogViewResWorkComponent } from '../dialog-view-res-work/dialog-view-res-work.component';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss'],
  providers: [GraphDataService]
})
export class GraphComponent implements OnInit {
  // config
  public dataSource: any;
  public chartConfig: any;
  // data
  public dataset: Dataset[] = [{ id: '1', data: [], seriesname: 'graph' }];
  public connectors: ConnectorsGraphData[] = [];
  public maxCountNode = 0;
  // clicked event object
  private choseNode: ObjEventClicNode;
  // private eventClickChartData: ObjEventClicOnChart;
  // state graph
  public stateArc: EnumGraphArcState = EnumGraphArcState.Directed;
  public StateArcSwith = true;
  public titleStateArc = 'направленный';
  // work
  public NodeForWork: number;
  public resultWork: ResultWork[] = [];
  // width screen
  public width = 0;



  constructor(
    private eventService: EventService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.width = window.innerWidth;
    this.eventService.resizeWidth
      .subscribe(
        next => {
          // console.log(next);
          this.width = next;
        }
      );
    this.maxCountNode = GraphDataService.getMaxCountNode();
    this.chartConfig = GraphDataService.getChartConfig();
    this.dataSource = GraphDataService.getDataSource(this.dataset, this.connectors);
  }

  public changeStateArc() {
    switch (this.stateArc) {
      case EnumGraphArcState.Directed: {
        this.stateArc = EnumGraphArcState.NonDirected;
        this.titleStateArc = 'не направленный';
        break;
      }
      case EnumGraphArcState.NonDirected: {
        this.stateArc = EnumGraphArcState.Directed;
        this.titleStateArc = 'направленный';
        break;
      }
    }
  }

  public doWork() {
    this.resultWork = GraphDataService.doWork(this.dataset, this.connectors, this.NodeForWork - 1,
      this.dataset[0].data.length, this.connectors[0].connector.length);
    if (this.width < 1200) {
      const dialogRef = this.dialog.open(DialogViewResWorkComponent, {
        width: 'auto',
        height: 'auto',
        data: this.resultWork
      });
    }
    // console.log('all result', this.resultWork);
  }

  public addNewNode() {
    GraphDataService.addNewNode(this.dataset);
  }

  public removeAllNode() {
    GraphDataService.removeAllNode(this.dataset, this.connectors);
    this.NodeForWork = null;
    this.resultWork = [];
  }

  public addArcForNode(event) {
    const obj = event as FusionChartsEvent;
    this.choseNode = {
      idNode: +obj.dataObj.id,
      nameNode: obj.dataObj.label
    };
    document.getElementById('hb').click();

  }

  openDialogAddArc() {
    const dialogData: DialogData = {
      idNode: this.choseNode.idNode,
      nameNode: this.choseNode.nameNode,
      listNodes: GraphDataService.getListAllNodeForCreateArc(this.dataset, this.choseNode),
    };
    const dialogRef = this.dialog.open(DialogNodeComponent, {
      width: 'auto',
      height: 'auto',
      data: dialogData
    });
    dialogRef.afterClosed().subscribe((result: ResultAddConnector) => {
      if (result != null) {
        GraphDataService.addConnectorToNode(this.connectors,
          this.choseNode.idNode, result.Node.idNode, result.valueConnector, this.stateArc);
      }
      this.choseNode = null;
    });

  }

  public changePositionNode(event) {
    const obj = event as FusionChartsEvent;
    const idDragNode = +obj.dataObj.id;
    const newPosX = obj.dataObj.x;
    const newPosY = obj.dataObj.y;
    GraphDataService.changePosNode(this.dataset, newPosX, newPosY, idDragNode);
  }

}
