import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogViewTaskComponent } from './dialog-view-task/dialog-view-task.component';

@Component({
  selector: 'app-task-block',
  templateUrl: './task-block.component.html',
  styleUrls: ['./task-block.component.scss']
})
export class TaskBlockComponent implements OnInit {
  public textTask = 'Алгоритм Беллмана-Форда позволяет найти кратчайшие пути из одной вершины графа до всех остальных, даже для графов, в которых веса ребер могут быть отрицательными. Тем не менее, в графе не должно быть циклов отрицательного веса, достижимых из начальной вершины, иначе вопрос о кратчайших путях является бессмысленным. При этом алгоритм Форда-Беллмана позволяет определить наличие циклов отрицательного веса, достижимых из начальной вершины. Алгоритм Форда-Беллмана использует динамическое программирование.';
  public textViewTask = 'Посмотрет задание';
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialogViewTask() {
    this.dialog.open(DialogViewTaskComponent, {
      width: 'auto',
      height: 'auto',
    });
  }

}
