import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-view-task',
  templateUrl: './dialog-view-task.component.html',
  styleUrls: ['./dialog-view-task.component.scss']
})
export class DialogViewTaskComponent implements OnInit {
  public text = 'Алгоритм Форда-Беллмана позволяет найти кратчайшие пути из одной вершины графа до всех остальных, даже для графов, в которых веса ребер могут быть отрицательными. Тем не менее, в графе не должно быть циклов отрицательного веса, достижимых из начальной вершины, иначе вопрос о кратчайших путях является бессмысленным. При этом алгоритм Форда-Беллмана позволяет определить наличие циклов отрицательного веса, достижимых из начальной вершины. Алгоритм Форда-Беллмана использует динамическое программирование.';

  constructor(
    public dialogRef: MatDialogRef<DialogViewTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
