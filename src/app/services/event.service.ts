import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class EventService {

    public resizeWidth: BehaviorSubject<number> = new BehaviorSubject<number>(window.innerWidth);

    constructor() {
        window.onresize = () => {
            const width = window.innerWidth;
            // console.log(width);
            this.resizeWidth.next(width);
        };

    }
}
